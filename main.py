import time
from datetime import datetime

import requests

from openapi_master import get_app_code
from openapi_master import get_app_token
from openapi_master import get_slid_user_token
from openapi_master import get_slnet_token
from openapi_master import auth
from openapi_master import get_user_id
from openapi_master import get_user_info
from auth_data import args, tb_token_starline, chat_id

common = {'update_time': 100}
now = datetime.now()
dt_string = now.strftime("%Y-%d-%m %H:%M:%S")
ts_last = ""


def telegram_send_message(position, msg):
    try:
        text = ''.join('[%s], %s' % (position, msg))

        url = 'https://api.telegram.org/bot{tb_token_starline}/sendMessage?chat_id={chat_id}&text={text}'.format(
            tb_token_starline=tb_token_starline, chat_id=chat_id, text=text)
        r = requests.get(url, headers={})
    except Exception as e:
        print(e)


def get_location():
    global ts_last
    while 1:
        try:
            time.sleep(common['update_time'])
            url = "https://developer.starline.ru/json/v1/device/862095053740228/position"

            if not args['slnet_token']:
                full_auth_starline()
                msg = "Получение slnet_token"
                telegram_send_message(None, msg)
            else:
                r = requests.get(url, headers={"Cookie": "slnet=" + args['slnet_token']})
                response = r.json()
                lat = response['device']['position']['lat']
                ts = response['device']['position']['ts']

                print(response['device'])
                print(ts_last)
                if not lat or lat == '' or ts == ts_last:
                    position = response['device']['position']
                    msg = "Местоположение недоступно"
                    telegram_send_message(position, msg)

                ts_last = ts
        except Exception as e:
            msg = 'error: %s [%s]' % (e, dt_string)
            telegram_send_message(None, msg)
            continue


def full_auth_starline():
    try:
        args['app_code'] = get_app_code.get_app_code(args['application_id'], args['application_secret'])
        args['app_token'] = get_app_token.get_app_token(args['application_id'], args['application_secret'],
                                                        args['app_code'])
        args['slid_user_token'] = get_slid_user_token.get_slid_user_token(args['app_token'], args['login'],
                                                                          args['password'])
        args['slnet_token'] = get_slnet_token.get_slnet_token(args['slid_user_token'])
        args['auth'] = auth.auth(args['application_id'], args['application_secret'], args['login'],
                                 args['password'])  # <--
        args['user_id'] = get_user_id.get_user_id(args['slid_user_token'])
    except:
        return False
    return args


if __name__ == '__main__':
    get_location()

